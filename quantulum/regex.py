#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""quantulum regex functions."""

# Standard library
import re

# Quantulum
from . import load as l

UNITS = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
         'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
         'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']

TENS = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
        'eighty', 'ninety']

SCALES = ['hundred', 'thousand', 'million', 'billion', 'trillion']


###############################################################################
def get_numwords():
    """Convert number words to integers in a given text."""
    numwords = {'and': (1, 0), 'a': (1, 1), 'an': (1, 1)}

    for idx, word in enumerate(UNITS):
        numwords[word] = (1, idx)
    for idx, word in enumerate(TENS):
        numwords[word] = (1, idx * 10)
    for idx, word in enumerate(SCALES):
        numwords[word] = (10 ** (idx * 3 or 2), 0)

    all_numbers = '|'.join(u'\\b%s\\b' % i for i in numwords.keys() if i)

    return all_numbers, numwords


###############################################################################

SUFFIXES = {'K': 1e3, 'M': 1e6, 'B': 1e9, 'T': 1e12}

UNI_SUPER = {u'¹': '1', u'²': '2', u'³': '3', u'⁴': '4', u'⁵': '5',
             u'⁶': '6', u'⁷': '7', u'⁸': '8', u'⁹': '9', u'⁰': '0'}

UNI_FRAC = {u'¼': '1/4', u'½': '1/2', u'¾': '3/4', u'⅐': '1/7', u'⅑': '1/9',
            u'⅒': '1/10', u'⅓': '1/3', u'⅔': '2/3', u'⅕': '1/5', u'⅖': '2/5',
            u'⅗': '3/5', u'⅘': '4/5', u'⅙': '1/6', u'⅚': '5/6', u'⅛': '1/8',
            u'⅜': '3/8', u'⅝': '5/8', u'⅞': '7/8'}

OPERATORS = {u'/': u' per ', u' per ': u' per ', u' a ': ' per ',
             u'*': u' ', u' ': u' ', u'·': u' ', u'x': u' '}

ALL_NUM, NUMWORDS = get_numwords()
FRACTIONS = re.escape(''.join(UNI_FRAC.keys()))
SUPERSCRIPTS = re.escape(''.join(UNI_SUPER.keys()))

MULTIPLIERS = '|'.join(u'%s' % re.escape(i) for i in OPERATORS if
                        OPERATORS[i] == ' ')

# Pattern for extracting a digit-based number:
# required number
#   optional sign
#   required digits
#   optional decimals
# optional exponent
#   multiplicative operators
#   required exponent prefix
#   required exponent, superscript or normal
# optional fraction

NUM_PATTERN = u'''

    (?:
        [+-]?
        \.?\\d+
        (?:\.\\d+)?
    )
    (?:
        (?:%s)?
        (?:E|e|10\^?)
        (?:[+-]?\\d+|[%s])
    )?
    (?:
        \ \\d+/\\d+|\ ?[%s]|/\\d+
    )?

''' % (MULTIPLIERS, SUPERSCRIPTS, FRACTIONS)

# Pattern for a range of numbers
# First number
# lookbehind, avoid "Area51"
# Second number
# Group for ranges or uncertainties

RAN_PATTERN = u'''

    (?:
        (?<![a-zA-Z0-9+.-])
        %s
    )
    (?:
        \ ?(?:(?:-\ )?to|-|and|\+/-|±)\ ?
    %s)?

''' % (NUM_PATTERN, NUM_PATTERN)

# Pattern for extracting mixed digit-spelled num
# lookbehind, avoid "Area51"
TXT_PATTERN = u'''
    (?:
        (?<![a-zA-Z0-9+.-])
        %s
    )?
    [ -]?(?:%s)
    [ -]?(?:%s)?[ -]?(?:%s)?[ -]?(?:%s)?
    [ -]?(?:%s)?[ -]?(?:%s)?[ -]?(?:%s)?
''' % tuple([NUM_PATTERN] + 7 * [ALL_NUM])

REG_TXT = re.compile(TXT_PATTERN, re.VERBOSE | re.IGNORECASE)


###############################################################################
def get_units_regex():
    """Build a compiled regex object."""
    op_keys = sorted(OPERATORS.keys(), key=len, reverse=True)
    unit_keys = sorted(l.UNITS.keys(), key=len, reverse=True)
    symbol_keys = sorted(l.SYMBOLS.keys(), key=len, reverse=True)

    exponent = u'(?:(?:\^?\-?[0-9%s]*)(?:\ cubed|\ squared)?)(?![a-zA-Z])' % \
               SUPERSCRIPTS

    all_ops = '|'.join([u'%s' % re.escape(i) for i in op_keys])
    all_units = '|'.join([u'%s' % re.escape(i) for i in unit_keys])
    all_symbols = '|'.join([u'%s' % re.escape(i) for i in symbol_keys])

    # Currencies, mainly
    # Number
    # Operator + Unit (1)
    # Operator + Unit (2)
    # Operator + Unit (3)
    # Operator + Unit (4)
    pattern = u'''

        (?P<prefix>(?:%s)(?![a-zA-Z]))?
        (?P<value>%s)-?
        (?:(?P<operator1>%s)?(?P<unit1>(?:%s)%s)?)
        (?:(?P<operator2>%s)?(?P<unit2>(?:%s)%s)?)
        (?:(?P<operator3>%s)?(?P<unit3>(?:%s)%s)?)
        (?:(?P<operator4>%s)?(?P<unit4>(?:%s)%s)?)

    ''' % tuple([all_symbols, RAN_PATTERN] + 4 * [all_ops, all_units,
                                                  exponent])

    regex = re.compile(pattern, re.VERBOSE | re.IGNORECASE)

    return regex

REG_DIM = get_units_regex()
