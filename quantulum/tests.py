#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""quantulum tests."""

from __future__ import print_function
from builtins import object # For Py 2/3 Compatibilty (next operator)

# Standard library
import os
import re
import json
import unittest

# Dependencies
import wikipedia

# Quantulum
from . import load as l
from . import parser as p
from . import classes as c

COLOR1 = '\033[94m%s\033[0m'
COLOR2 = '\033[91m%s\033[0m'
TOPDIR = os.path.dirname(__file__) or "."


################################################################################
# def embed_text(quants, beg_char, chunk, content):
#     """Embed quantities in text."""
#     if quants:
#         end_char = max((chunk + 1) * 1000, quants[-1].span[1])
#         text = content[beg_char:end_char]
#         shift = 0
#         for quantity in quants:
#             index = quantity.span[1] - beg_char + shift
#             to_add = COLOR1 % (' {' + str(quantity) + '}')
#             text = text[0:index] + to_add + COLOR2 % text[index:]
#             shift += len(to_add) + len(COLOR2) - 6
#     else:
#         end_char = (chunk + 1) * 1000
#         text = content[beg_char:end_char]

#     return text, end_char


# ###############################################################################
# def wiki_test(page='CERN'):
#     """Download a wikipedia page and test the parser on its content.

#     Pages full of units:
#         CERN
#         Hubble_Space_Telescope,
#         Herschel_Space_Observatory
#     """
#     content = wikipedia.page(page).content
#     parsed = p.parse(content)
#     parts = int(round(len(content) * 1.0 / 1000))

#     print
#     end_char = 0
#     for num, chunk in enumerate(range(parts)):
#         _ = os.system('clear')
#         print
#         quants = [j for j in parsed if chunk * 1000 < j.span[0] < (chunk + 1) *
#                   1000]
#         beg_char = max(chunk * 1000, end_char)
#         text, end_char = embed_text(quants, beg_char, chunk, content)
#         print COLOR2 % text
#         print
#         try:
#             _ = raw_input('--------- End part %d of %d\n' % (num + 1, parts))
#         except (KeyboardInterrupt, EOFError):
#             return


###############################################################################


class EndToEndTests(unittest.TestCase):
    """Test suite for the quantulum project."""

    def setUp(self):
        """Load all tests from tests.json."""
        path = os.path.join(TOPDIR, 'tests.json')
        self.test_file = open(path)
        self.tests = json.load(self.test_file)

        for test in self.tests:
            res = []
            for item in test['res']:
                quantity = self.get_quantity(test, item)
                if quantity is None:
                    return
                res.append(quantity)
            test['res'] = [i for i in res]

    def tearDown(self):
        self.test_file.close()


    def test_parse(self):
        """Test for parser.parse() function."""
        tests_failed = 0
        for test in sorted(self.tests, key=lambda x: len(x['req'])):
            res = p.parse(test['req'])
            self.assertEqual(res, test['res'], self.get_error_msg(
                res, test['res']))

    def get_error_msg(self, results, test_results):
        msg = '\nExpected: %s\nGot: %s' % (test_results, results)
        for i, res in enumerate(results):
            test_res = test_results[i]
            if res == test_res:
                continue

            msg += '\n\tExpected Quantity %s, Got %s' % (test_res, res)
            if res.value != test_res.value:
                msg += '\n\t\tWrong Value (expected %g got %g)\n' % (
                    res.value, test_res.value)

            expected_unit = test_res.unit
            res_unit = res.unit
            if res_unit != expected_unit:
                msg += '\n\t\tExpected %s\n\t\tGot %s' % (
                    expected_unit, res_unit)

            if res.surface != test_res.surface:
                msg += '\n\t\tExpected surface %s, Got %s' % (
                    res.surface, test_res.surface)

            if res.span != test_res.span:
                msg += '\n\t\tExpected span %s, Got %s' % (
                    res.span, test_res.span)

            if res.uncertainty != test_res.uncertainty:
                msg += '\n\t\tExpected uncertainty %s, Got %s' % (
                    res.uncertainty, test_res.uncertainty)
        return msg

    def get_quantity(self, test, item):
        """Build a single quantity for the test."""
        try:
            unit = l.NAMES[item['unit']]
        except KeyError:
            try:
                entity = item['entity']
            except KeyError:
                print('Could not find %s, provide "dimensions" and'
                    ' "entity"' % item['unit'])
                return
            if entity == 'unknown':
                dimensions = [{'base': l.NAMES[i['base']].entity.name,
                            'power': i['power']} for i in
                            item['dimensions']]
                entity = c.Entity(name='unknown', dimensions=dimensions)
            elif entity in l.ENTITIES:
                entity = l.ENTITIES[entity]
            else:
                print('Could not find %s, provide "dimensions" and'
                    ' "entity"' % item['unit'])
                return
            unit = c.Unit(name=item['unit'],
                        dimensions=item['dimensions'],
                        entity=entity)
        try:
            span = next(re.finditer(re.escape(item['surface']),
                                    test['req'])).span()
        except StopIteration:
            print('Surface mismatch for "%s"' % test['req'])
            return

        uncert = None
        if 'uncertainty' in item:
            uncert = item['uncertainty']

        quantity = c.Quantity(value=item['value'],
                            unit=unit,
                            surface=item['surface'],
                            span=span,
                            uncertainty=uncert)

        return quantity


###############################################################################
if __name__ == '__main__':
    # Set USE_CLF = True in classifier.py for all tests to pass
    unittest.main()
